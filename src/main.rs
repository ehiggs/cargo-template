// `error_chain!` can recurse deeply
// #![recursion_limit = "1024"]i

extern crate docopt;
#[macro_use]
extern crate error_chain;
extern crate git2;
extern crate handlebars;
extern crate rustc_serialize;
extern crate tempdir;
extern crate toml;
extern crate walkdir;

use std::env;
use std::io;
use std::process;

use docopt::Docopt;

mod cmd;
mod config;
mod errors;
mod repo;
mod template;
mod util;
use errors::{Error, ErrorKind, Result};

macro_rules! wout {
    ($($arg:tt)*) => ({
        use std::io::Write;
        (writeln!(&mut ::std::io::stdout(), $($arg)*)).unwrap();
    });
}

macro_rules! werr {
    ($($arg:tt)*) => ({
        use std::io::Write;
        (writeln!(&mut ::std::io::stderr(), $($arg)*)).unwrap();
    });
}

macro_rules! fail {
    ($e:expr) => (Err(::std::convert::From::from($e)));
}

macro_rules! command_list {
    () => (
"
    new         Runs the new command.
    list        Lists available templates.
    update      Update a template or all templates.
"
    )
}

static USAGE: &'static str = concat!("
Usage:
    cargo-template <command> [<args>...]
    cargo-template [options]

Options:
    --list           List all commands available.
    -h, --help       Display this message
    --version    Print version info and exit

Commands:", command_list!());

#[derive(RustcDecodable)]
struct Args {
    arg_command: Option<Command>,
    flag_list: bool,
}

fn main() {
    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.options_first(true)
                                           .version(Some(util::version()))
                                           .decode())
                            .unwrap_or_else(|e| e.exit());
    if args.flag_list {
        wout!(concat!("Installed commands:", command_list!()));
        return;
    }
    match args.arg_command {
        None => {
            werr!(concat!(
                "{{name}} is <your description here>
Please choose one of the following commands:",
                command_list!()));
            process::exit(0);
        }
        Some(cmd) => {
            match cmd.run() {
                Ok(()) => process::exit(0),
                Err(Error(ErrorKind::Flag(err), _)) => err.exit(),
                Err(Error(ErrorKind::Io(ref err), _))
                        if err.kind() == io::ErrorKind::BrokenPipe => {
                    process::exit(0);
                }
                Err(Error(ErrorKind::Io(err), _)) => {
                    werr!("{}", err);
                    process::exit(1);
                }
                Err(Error(ErrorKind::Msg(msg), _)) => {
                    werr!("{}", msg);
                    process::exit(1);
                }
                Err(Error(ErrorKind::RenderError(msg), _)) => {
                    werr!("{}", msg);
                    process::exit(1);
                }
                Err(Error(ErrorKind::TemplateRenderError(msg), _)) => {
                    werr!("{}", msg);
                    process::exit(1);
                }
            }
        }
    }
}

#[derive(Debug, RustcDecodable)]
enum Command {
    New,
    List,
    Update,
}

impl Command {
    fn run(self) -> Result<()> {
        let argv: Vec<_> = env::args().map(|v| v.to_owned()).collect();
        let argv: Vec<_> = argv.iter().map(|s| &**s).collect();
        let argv = &*argv;
        match self {
            Command::New => cmd::new::run(argv),
            Command::List=> cmd::list::run(argv),
            Command::Update=> cmd::update::run(argv),
        }
    }
}
