use git2::Repository;

pub fn clone_into(url: &str, dest: &str) -> Repository {
    let repo = match Repository::clone(url, dest) {
            Ok(repo) => repo,
                Err(e) => panic!("failed to clone: {}", e),
    };
    repo
}

