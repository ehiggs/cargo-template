use std::collections::BTreeMap;
use std::path;
use std::env;
use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};
use errors::{Result, ResultExt};
use config;
use util;
use handlebars::{Handlebars, Context, no_escape};
use walkdir::{DirEntry, WalkDir, WalkDirIterator};
use template;

static USAGE: &'static str = "
Runs the new command

Usage:
   cargo-template new <template-name> <name>

Common options:
    -h, --help    Display this message
";


#[derive(RustcDecodable)]
struct Args {
    arg_template_name: String,
    arg_name: String,
}

pub fn run(argv: &[&str]) -> Result<()> {
    let args: Args = try!(util::get_args(USAGE, argv));
    args.new()
}

impl Args {
    fn new(&self) -> Result<()> {
		let path = PathBuf::from(&self.arg_name);
        let cfg = try!(config::get_or_create_config().chain_err(||"Could not load config"));
        let user = cfg.user;
        // construct the mapping used to populate the template
        // if in the future we want to make more varaibles available in
        // the templates, this would be the place to do it.
        let mut handlebars = Handlebars::new();
        // We don't want html escaping unless users explicitly ask for it...
        handlebars.register_escape_fn(no_escape);
        handlebars.register_helper("toml-escape", Box::new(template::toml_escape_helper));
        handlebars.register_helper("html-escape", Box::new(template::html_escape_helper));

        let mut data = BTreeMap::new();
        data.insert("name".to_owned(), self.arg_name.clone());
        data.insert("author-name".to_owned(), user.name.clone());
        data.insert("author-email".to_owned(), user.email.clone());

        let template_set = try!(get_input_template(&self.arg_template_name)
                                .chain_err(|| {
                                    format!("Could not find input template: {}", 
                                            self.arg_template_name)
                                }));
        for template in template_set.template_files.iter() {
            let template_str = try!(template.template());
            let dest_path = &path.join(template.path());

            // Skip files that already exist.
            if fs::metadata(&dest_path).is_ok() {
                continue;
            }

            let parent = try!(dest_path.parent()
                              .ok_or(
                                  format!("failed to make sure parent directory \
                                             exists for {}", dest_path.display())
                          ));
            try!(fs::create_dir_all(&parent)
                 .chain_err(|| {
                     format!("failed to create path to destination file {}",
                             parent.display())
                 }));

            // create the new file & render the template to it
            let mut dest_file = try!(File::create(&dest_path).chain_err(|| {
                format!("failed to open file for writing: {}",
                        dest_path.display())
            }));
            try!(handlebars.template_renderw(&template_str, &Context::wraps(&data), &mut dest_file)
                 .chain_err(|| {
                     format!("Failed to render template for file: {}", dest_path.display())
                 }))
        }
        Ok(())
    }
}

fn get_input_template(name: &AsRef<Path>) -> Result<template::TemplateSet> {
    let templates_dir = try!(template::get_templates_dir());
    let template_dir = templates_dir.join(name);
    let template_files = try!(collect_template_dir(&template_dir));
    Ok(template::TemplateSet{ 
        template_dir: Some(template::TemplateDirectory::Normal(template_dir)),
        template_files: template_files
    })
}


fn collect_template_dir(template_path: &PathBuf) -> Result<Vec<Box<template::TemplateFile>>> {
    let mut templates = Vec::<Box<template::TemplateFile>>::new();                        
    // For every file found inside the given template directory, compile it as a handlebars
    // template and render it with the above data to a new file inside the target directory
    let mut it = WalkDir::new(template_path.clone()).into_iter();
    loop {
        let entry = match it.next() {
            None => break,
            Some(Err(err)) => bail!("Error! {}", err),
            Some(Ok(entry)) => entry
        };
        if entry.file_type().is_dir() { 
            if entry.file_name() == ".git" {
                it.skip_current_dir();
            }
            continue; 
        }
        // we don't care about the actual directory entries.
        let entry_path = entry.path();                                          
        let dest_file_name = PathBuf::from(try!(entry_path.strip_prefix(&template_path)
                                                .chain_err(|| {                             
                                                    format!("entry is somehow not a subpath \
                                                     of the directory being walked.")
                                                })));                                         
        templates.push(Box::new(template::InputFileTemplateFile::new(entry_path.to_path_buf(),          
                                                                     dest_file_name.to_path_buf())));
    };                                                                        
    Ok(templates)                                                               
}   
