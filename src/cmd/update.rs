use util;
use config::get_config;
use errors::{Result, ResultExt};
use walkdir::WalkDir;
use template::get_templates_dir;

static USAGE: &'static str = "
Runs the update command

Usage:
   cargo-template update [template-name...]

Common options:
    -h, --help    Display this message
";


#[derive(RustcDecodable)]
struct Args {
    arg_template_names: Vec<String>,
}

pub fn run(argv: &[&str]) -> Result<()> {
    let args: Args = try!(util::get_args(USAGE, argv));
    args.update()
}

impl Args {
    fn update(&self) -> Result<()> {
        let config = try!(get_config().chain_err(||
            "Could not load the config file."));
        let template_dir = try!(get_templates_dir().chain_err(||
            "Could not determine template directory"));
        if !template_dir.exists() {
            bail!(format!("Template directory \"{}\" does not exist", 
                template_dir.display()));
        }

        let available_repos = config.sources.keys();
        let filter = if self.arg_template_names.len() > 0 {
            for (source_name, source_config) in config.sources.iter()
                .filter(|&(sn, sc)| self.arg_template_names.contains(sn)) {
                println!("Updating: {}", source_name);
                // go to directory and perform update...
            }
        } else {
            for (source_name, source_config) in config.sources.iter() {
                println!("Updating: {}", source_name);
                // go to directory and perform update...
            }
        };
        Ok(())
    }
}
