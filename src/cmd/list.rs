use util;
use errors::{Result, ResultExt};
use walkdir::WalkDir;
use template::get_templates_dir;

static USAGE: &'static str = "
Runs the list command

Usage:
   cargo-template list

Common options:
    -h, --help    Display this message
";


#[derive(RustcDecodable)]
struct Args {
}

pub fn run(argv: &[&str]) -> Result<()> {
    let args: Args = try!(util::get_args(USAGE, argv));
    args.list()
}

impl Args {
    fn list(&self) -> Result<()> {
        let template_dir = try!(get_templates_dir().chain_err(||"Could not determine template directory"));
        if !template_dir.exists() {
            bail!(format!("Template directory \"{}\" does not exist", template_dir.display()));
        }
        for entry in WalkDir::new(template_dir.clone()).min_depth(2).max_depth(2) {
            let entry = entry.unwrap();
            if let Ok(repo_name) = entry.path().strip_prefix(&template_dir) {
                println!("{}", repo_name.display());
            }
        }
        Ok(())
    }
}
