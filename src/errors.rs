use docopt;
use handlebars;
use std::io;

error_chain! { 
    foreign_links {
        Io(io::Error);
        Flag(docopt::Error);
        TemplateRenderError(handlebars::TemplateRenderError);
        RenderError(handlebars::RenderError);
    }
}
