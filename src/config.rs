use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use std::collections::BTreeMap;
use std::env;
use std::result::Result as StdResult;
use std::env::{home_dir, var};
use errors::{Result, ResultExt};
use toml;
use git2::Config as GitConfig;
use rustc_serialize::{Decodable, Decoder};

static CONFIG_NAME: &'static str = "CargoTemplate.toml";
static PROJECT_NAME: &'static str = "cargo-template";

#[derive(Debug, RustcDecodable, Clone, Default)]
pub struct Config {
    pub user: UserConfig,
    pub sources: BTreeMap<String, SourceConfig>
}

#[derive(Debug, RustcDecodable, Clone, Default)]
pub struct UserConfig {
    pub name: String,
    pub email: String
}

/// A SourceKind defines the behaviour of the template repository.
/// A Git repository pulls from a probably remote machine to a local template directory.
/// Future kinds could include mercurial, zip files, or simple directories.
#[derive(Debug, Clone)]
pub enum SourceKind {
    Git
}

impl Default for SourceKind {
    fn default() -> SourceKind {
        SourceKind::Git
    }
}

impl Decodable for SourceKind {
    fn decode<D: Decoder>(d: &mut D) -> StdResult<SourceKind, D::Error> {
        d.read_enum("kind", |d| {
            d.read_enum_variant(&["git"], |d, _| {
                Ok(SourceKind::Git)
            })
        })
    }
}

#[derive(Debug, Clone, RustcDecodable)]
pub enum SourceConfig {
    Simple(String),
    Detailed(DetailedSourceConfig)
}

impl SourceConfig {
    pub fn update(&self) -> Result<()> {
        Ok(())
    }
}

#[derive(Debug, RustcDecodable, Clone, Default)]
pub struct DetailedSourceConfig {
    kind: Option<SourceKind>,
    uri: String
}
pub fn create_config(name: &str, email: &str) -> Result<()> {
    let config_dir = try!(get_config_dir());
    let config_path = config_dir.join(CONFIG_NAME);
    let mut file = try!(File::create(config_path)
                    .chain_err(|| "Could not create config file: {}"));
    write!(file, r#"[user]
name = "{}"
email = "{}"

[sources]
"#, name, email);
    Ok(())
}

fn discover_author() -> Result<(String, Option<String>)> {
    let git_config = GitConfig::open_default().ok();
    let git_config = git_config.as_ref();
    let name_variables = ["GIT_AUTHOR_NAME", "GIT_COMMITTER_NAME",
                         "USER", "USERNAME", "NAME"];
    let name = get_environment_variable(&name_variables[0..2])
                        .or_else(|| git_config.and_then(|g| g.get_string("user.name").ok()))
                        .or_else(|| get_environment_variable(&name_variables[3..]));

    let name = match name {
        Some(name) => name,
        None => {
            let username_var = if cfg!(windows) {"USERNAME"} else {"USER"};
            bail!("could not determine the current user, please set ${}",
                  username_var)
        }
    };
    let email_variables = ["GIT_AUTHOR_EMAIL", "GIT_COMMITTER_EMAIL", "EMAIL"];
    let email = get_environment_variable(&email_variables[0..2])
                          .or_else(|| git_config.and_then(|g| g.get_string("user.email").ok()))
                          .or_else(|| get_environment_variable(&email_variables[3..]));

    let name = name.trim().to_string();
    let email = email.map(|s| s.trim().to_string());

    Ok((name, email))
}

fn get_environment_variable(variables: &[&str] ) -> Option<String>{
    variables.iter()
        .filter_map(|var| env::var(var).ok())
        .next()
}

pub fn get_or_create_config() -> Result<Config> {
    let config_dir = try!(get_config_dir());
    let config_file = config_dir.join(CONFIG_NAME);
    match load_config(&config_file) {
        Ok(cfg_str) => parse_config(&cfg_str),
        Err(e) => {
            let (name, email) = try!(discover_author()
                                     .chain_err(|| "Could not load config and failed \
                                                to create a new one as we could not \
                                                figure out the name or email settings"));

            let email = try!(email.ok_or(format!("Could not figure out the email address when \
                                    creating config file. Please add an \"email\" entry \
                                    in {}", config_file.display())));
             create_config(&name, &email);
             Ok(Config{user: UserConfig{name: name, email: email}, sources: BTreeMap::new()}) 
        }
    }
}

fn load_config(path : &Path) -> Result<String> {
    let mut contents = String::new();
    try!(File::open(&path).and_then(|mut x| x.read_to_string(&mut contents))
         .chain_err(|| 
                    format!("Could not open config file: \"{}\"", 
                            path.display())));
    Ok(contents)
}

fn parse_config(toml_str: &str) -> Result<Config> {
    let decoded: Config = try!(toml::decode_str(&toml_str).ok_or("Coult not parse config file."));
    Ok(decoded)
}

pub fn get_config() -> Result<Config> {
    let config_dir = try!(get_config_dir());
    let config_file = config_dir.join(CONFIG_NAME);
    let toml_str = try!(load_config(&config_file));
    parse_config(&toml_str)
}

pub fn get_config_dir() -> Result<PathBuf> {

    let default_config_basedir = try!(home_dir().ok_or("Could not find home directory".to_owned()));
    let default_config_basedir = default_config_basedir.join(".config");
    match var("XDG_CONFIG_HOME") {
        Ok(ref base_dir) if base_dir == "" => Ok(default_config_basedir.join(PROJECT_NAME)),
        Ok(ref base_dir) => Ok(PathBuf::from(base_dir).join(PROJECT_NAME)),
        _ => Ok(default_config_basedir.join(PROJECT_NAME))
    }
}


